package ru.aushakov.tm.enumerated;

public enum Status {

    PLANNED("Planned"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
